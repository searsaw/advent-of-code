package main

import (
	"fmt"
	"strconv"
	"time"

	"github.com/searsaw/advent-of-code/2017/star27/knothash"
)

var input = "ljoxqyyw"

func elapsed(what string) func() {
	start := time.Now()

	return func() {
		fmt.Printf("%s took %v\n", what, time.Since(start))
	}
}

func countOnes(binary string) int {
	total := 0

	for i := 0; i < len(binary); i++ {
		if string(binary[i]) == "1" {
			total++
		}
	}

	return total
}

func numberOfSetBits(i int) int {
	i = i - ((i >> 1) & 0x55555555)
	i = (i & 0x33333333) + ((i >> 2) & 0x33333333)

	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24
}

func main() {
	defer elapsed("main")()
	total := 0

	for i := 0; i < 128; i++ {
		knotHash := knothash.GetKnotHash(input + "-" + strconv.Itoa(i))

		for i := 0; i < len(knotHash); i++ {
			total += numberOfSetBits(knotHash[i])
		}
	}

	fmt.Printf("The total spaces used is %d.\n", total)
}
