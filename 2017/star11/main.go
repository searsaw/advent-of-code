package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

var input = "5	1	10	0	1	7	13	14	3	12	8	10	7	12	0	6"

func stringToIntSlice(s string) []int {
	numbers := make([]int, 16)

	for i, s := range strings.Split(input, "\t") {
		n, _ := strconv.Atoi(s)
		numbers[i] = n
	}

	return numbers
}

func intToStringSlice(numbers []int) string {
	ss := make([]string, 16)

	for _, number := range numbers {
		s := strconv.Itoa(number)
		ss = append(ss, s)
	}

	return strings.Join(ss, "\t")
}

func getIndexOfGreatest(numbers []int) int {
	greatestIndex := 0

	for index, number := range numbers {
		if number > numbers[greatestIndex] {
			greatestIndex = index
		}
	}

	return greatestIndex
}

func contains(needle string, haystack []string) bool {
	for _, i := range haystack {
		if needle == i {
			return true
		}
	}

	return false
}

func elapsed(what string) func() {
	start := time.Now()

	return func() {
		fmt.Printf("%s took %v\n", what, time.Since(start))
	}
}

func main() {
	defer elapsed("main")()
	pastCounts := make([]string, 20)
	currentCounts := input
	blockCounts := stringToIntSlice(input)
	cycles := 0

	for !contains(currentCounts, pastCounts) {
		pastCounts = append(pastCounts, currentCounts)
		greatestIndex := getIndexOfGreatest(blockCounts)
		greatestCount := blockCounts[greatestIndex]

		for i := 0; i < greatestCount; i++ {
			blockCounts[greatestIndex] = 0
			blockCounts[(greatestIndex+i+1)%len(blockCounts)]++
		}

		cycles++
		currentCounts = intToStringSlice(blockCounts)
	}

	fmt.Printf("The number of cycles is %d\n", cycles)
}
