package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

var input = `0: 4
1: 2
2: 3
4: 4
6: 8
8: 5
10: 8
12: 6
14: 6
16: 8
18: 6
20: 6
22: 12
24: 12
26: 10
28: 8
30: 12
32: 8
34: 12
36: 9
38: 12
40: 8
42: 12
44: 17
46: 14
48: 12
50: 10
52: 20
54: 12
56: 14
58: 14
60: 14
62: 12
64: 14
66: 14
68: 14
70: 14
72: 12
74: 14
76: 14
80: 14
84: 18
88: 14`

// Direction defines the direction a scanner is going
type Direction int

// I am setting constants. Woohoo
const (
	Inc Direction = iota
	Dec
)

// Layer describes a layer in the firewall
type Layer struct {
	Direction       Direction
	Range           int
	ScannerPosition int
}

func elapsed(what string) func() {
	start := time.Now()

	return func() {
		fmt.Printf("%s took %v\n", what, time.Since(start))
	}
}

func processInput() []Layer {
	var layers []Layer

	for _, data := range strings.Split(input, "\n") {
		dataSplit := strings.Split(data, ": ")
		layerNumber, _ := strconv.Atoi(dataSplit[0])
		scannerRange, _ := strconv.Atoi(dataSplit[1])

		if len(layers) < layerNumber {
			for i := len(layers); i < layerNumber; i++ {
				layer := Layer{Inc, 0, 0}
				layers = append(layers, layer)
			}
		}

		layer := Layer{Inc, scannerRange, 0}
		layers = append(layers, layer)
	}

	return layers
}

func main() {
	defer elapsed("main")()
	layers := processInput()
	severity := 0

	for depth, layer := range layers {
		if layer.ScannerPosition == 0 {
			severity += depth * layer.Range
		}

		for i := 0; i < len(layers); i++ {
			if layers[i].Range != 0 {
				if layers[i].Direction == Inc {
					if layers[i].ScannerPosition+1 >= layers[i].Range {
						layers[i].Direction = Dec
						layers[i].ScannerPosition--
					} else {
						layers[i].ScannerPosition++
					}
				} else {
					if layers[i].ScannerPosition-1 < 0 {
						layers[i].Direction = Inc
						layers[i].ScannerPosition++
					} else {
						layers[i].ScannerPosition--
					}
				}
			}
		}
	}

	fmt.Printf("The trip severity is %d.\n", severity)
}
