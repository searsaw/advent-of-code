package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

var input = "183,0,31,146,254,240,223,150,2,206,161,1,255,232,199,88"

func elapsed(what string) func() {
	start := time.Now()

	return func() {
		fmt.Printf("%s took %v\n", what, time.Since(start))
	}
}

func processInput() []int {
	splits := strings.Split(input, ",")
	ints := make([]int, len(splits))

	for i, s := range splits {
		number, _ := strconv.Atoi(s)

		ints[i] = number
	}

	return ints
}

func createInitialHash() []int {
	sequence := make([]int, 256)

	for i := 0; i < 256; i++ {
		sequence[i] = i
	}

	return sequence
}

func reverse(sequence []int) []int {
	reversed := make([]int, len(sequence))

	for i, s := range sequence {
		reversed[len(sequence)-1-i] = s
	}

	return reversed
}

func concat(pieces ...[]int) []int {
	var sequence []int

	for _, outside := range pieces {
		for _, inside := range outside {
			sequence = append(sequence, inside)
		}
	}

	return sequence
}

func main() {
	defer elapsed("main")()
	lengths := processInput()
	hash := createInitialHash()
	currentIndex := 0
	skipSize := 0

	for _, length := range lengths {
		if currentIndex+length > len(hash) {
			lengthUntilEnd := len(hash) - currentIndex
			lengthFromBeginning := length - lengthUntilEnd
			sliceToReverse := concat(hash[currentIndex:], hash[0:lengthFromBeginning])
			reversed := reverse(sliceToReverse)
			hash = concat(reversed[lengthUntilEnd:], hash[lengthFromBeginning:currentIndex], reversed[0:lengthUntilEnd])
		} else {
			reversed := reverse(hash[currentIndex : currentIndex+length])
			hash = concat(hash[0:currentIndex], reversed, hash[currentIndex+length:])
		}

		currentIndex += length + skipSize

		if currentIndex > len(hash) {
			currentIndex = currentIndex % len(hash)
		}

		skipSize++
	}

	fmt.Printf("The first two numbers multiplied together equal %d.\n", hash[0]*hash[1])
}
