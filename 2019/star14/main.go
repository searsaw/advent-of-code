package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/searsaw/advent-of-code/2019/pkg/intcode"
)

const (
	input = "3,8,1001,8,10,8,105,1,0,0,21,34,51,64,73,98,179,260,341,422,99999,3,9,102,4,9,9,1001,9,4,9,4,9,99,3,9,1001,9,4,9,1002,9,3,9,1001,9,5,9,4,9,99,3,9,101,5,9,9,102,5,9,9,4,9,99,3,9,101,5,9,9,4,9,99,3,9,1002,9,5,9,1001,9,3,9,102,2,9,9,101,5,9,9,1002,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,99"
)

func main() {
	parts := strings.Split(input, ",")
	memory := make([]int, len(parts))
	for i, part := range parts {
		n, _ := strconv.Atoi(part)
		memory[i] = n
	}

	largestOutput := 0
	for _, ps := range getPossibleCombos([]int{5, 6, 7, 8, 9}) {
		mm := getMemoryCopies(memory)

		aOut, all, _ := getOutputSignal(mm[0], 0, ps[0], 0)
		bOut, bll, _ := getOutputSignal(mm[1], 0, ps[1], aOut)
		cOut, cll, _ := getOutputSignal(mm[2], 0, ps[2], bOut)
		dOut, dll, _ := getOutputSignal(mm[3], 0, ps[3], cOut)
		eOut, ell, _ := getOutputSignal(mm[4], 0, ps[4], dOut)

		signal := eOut
		ll := []int{all, bll, cll, dll, ell}
		for {
			aOut, all, err := getOutputSignal(mm[0], ll[0], signal)
			if errors.Is(err, intcode.ErrHalted) {
				break
			}
			if err != nil && !errors.Is(err, intcode.ErrHalted) {
				handleError(err)
			}
			ll[0] = all
			bOut, bll, err := getOutputSignal(mm[1], ll[1], aOut)
			if err != nil && !errors.Is(err, intcode.ErrHalted) {
				handleError(err)
			}
			ll[1] = bll
			cOut, cll, err := getOutputSignal(mm[2], ll[2], bOut)
			if err != nil && !errors.Is(err, intcode.ErrHalted) {
				handleError(err)
			}
			ll[2] = cll
			dOut, dll, err := getOutputSignal(mm[3], ll[3], cOut)
			if err != nil && !errors.Is(err, intcode.ErrHalted) {
				handleError(err)
			}
			ll[3] = dll
			eOut, ell, err := getOutputSignal(mm[4], ll[4], dOut)
			if err != nil && !errors.Is(err, intcode.ErrHalted) {
				handleError(err)
			}
			ll[4] = ell
			signal = eOut
		}
		if signal > largestOutput {
			largestOutput = signal
		}
	}

	fmt.Printf("the largest output is %d\n", largestOutput)
}

func getMemoryCopies(memory []int) [][]int {
	mm := [][]int{
		make([]int, len(memory)),
		make([]int, len(memory)),
		make([]int, len(memory)),
		make([]int, len(memory)),
		make([]int, len(memory)),
	}

	copy(mm[0], memory)
	copy(mm[1], memory)
	copy(mm[2], memory)
	copy(mm[3], memory)
	copy(mm[4], memory)

	return mm
}

func getPossibleCombos(values []int) [][]int {
	if len(values) == 1 {
		return [][]int{values}
	}

	var combos [][]int

	for i, v := range values {
		var vs []int
		vs = append(vs, values[:i]...)
		vs = append(vs, values[i+1:]...)

		for _, c := range getPossibleCombos(vs) {
			fc := []int{v}
			fc = append(fc, c...)
			combos = append(combos, fc)
		}
	}

	return combos
}

func handleError(err error) {
	fmt.Printf("error looking for the strongest signal: %s", err)
	os.Exit(1)
}

func getOutputSignal(memory []int, lastLocation int, input ...int) (int, int, error) {
	o, l, err := intcode.Run(memory, lastLocation, input)
	if err != nil {
		return o, l, fmt.Errorf("error running the intcode computer: %w", err)
	}

	return o, l, nil
}
