package intcode

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

const (
	OpAdd         = 1
	OpMultiply    = 2
	OpInput       = 3
	OpOutput      = 4
	OpJumpIfTrue  = 5
	OpJumpIfFalse = 6
	OpLessThan    = 7
	OpEquals      = 8
	OpStop        = 99

	ParamPosition  = 0
	ParamImmediate = 1
)

var (
	ErrHalted = errors.New("halted")
)

type Program struct {
	position int
	memory   []int
}

func NewProgram(input string) *Program {
	parts := strings.Split(input, ",")
	memory := make([]int, len(parts))
	for i, part := range parts {
		n, _ := strconv.Atoi(part)
		memory[i] = n
	}
	return &Program{
		position: 0,
		memory:   memory,
	}
}

func (p *Program) Run(inputs []int) (int, error) {
	nextInput := 0

	for {
		opcode := p.memory[p.position] % 100
		rest := p.memory[p.position] / 100

		if opcode == OpStop {
			return p.memory[0], ErrHalted
		}

		switch opcode {
		case OpAdd:
			codes := getParamCodes(rest, 2)
			value := p.getFromMemory(codes[0], p.memory[p.position+1]) + p.getFromMemory(codes[1], p.memory[p.position+2])
			p.memory[p.memory[p.position+3]] = value
			p.position += 4
		case OpMultiply:
			codes := getParamCodes(rest, 2)
			value := p.getFromMemory(codes[0], p.memory[p.position+1]) * p.getFromMemory(codes[1], p.memory[p.position+2])
			p.memory[p.memory[p.position+3]] = value
			p.position += 4
		case OpInput:
			p.memory[p.memory[p.position+1]] = inputs[nextInput]
			nextInput++
			p.position += 2
		case OpOutput:
			codes := getParamCodes(rest, 1)
			p.position++
			return p.getFromMemory(codes[0], p.memory[p.position+1]), nil
		case OpJumpIfTrue:
			codes := getParamCodes(rest, 2)
			if p.getFromMemory(codes[0], p.memory[p.position+1]) != 0 {
				p.position = p.getFromMemory(codes[1], p.memory[p.position+2])
				continue
			}
			p.position += 3
		case OpJumpIfFalse:
			codes := getParamCodes(rest, 2)
			if p.getFromMemory(codes[0], p.memory[p.position+1]) == 0 {
				p.position = p.getFromMemory(codes[1], p.memory[p.position+2])
				continue
			}
			p.position += 3
		case OpLessThan:
			codes := getParamCodes(rest, 2)
			value := 0
			if p.getFromMemory(codes[0], p.memory[p.position+1]) < p.getFromMemory(codes[1], p.memory[p.position+2]) {
				value = 1
			}
			p.memory[p.memory[p.position+3]] = value
			p.position += 4
		case OpEquals:
			codes := getParamCodes(rest, 2)
			value := 0
			if p.getFromMemory(codes[0], p.memory[p.position+1]) == p.getFromMemory(codes[1], p.memory[p.position+2]) {
				value = 1
			}
			p.memory[p.memory[p.position+3]] = value
			p.position += 4
		default:
			return 0, fmt.Errorf("opcode '%d' not allowed", opcode)
		}
	}
}

func (p *Program) Seek(position int) {
	p.position = position
}

func (p *Program) ReplaceInMemory(index, value int) {
	p.memory[index] = value
}

func (p *Program) getFromMemory(paramcode, key int) int {
	switch paramcode {
	case ParamPosition:
		return p.memory[key]
	case ParamImmediate:
		return key
	default:
		return 0
	}
}

func getParamCodes(rest, n int) []int {
	codes := make([]int, n)

	for i := 0; i < n; i++ {
		codes[i] = rest % 10
		rest /= 10
	}

	return codes
}
