const isOpenSpace = (x, y) => {
  const FAVE_NUMBER = 1364;
  const z = (x * x) + (3 * x) + (2 * x * y) + y + (y * y) + FAVE_NUMBER;
  const binary = z.toString(2);
  const numberOfOnes = binary.split('').reduce((tot, i) => i === '1' ? tot + 1 : tot, 0);

  return numberOfOnes % 2 === 0;
};

const isValidSpace = (x, y, visited) =>
  !visited.includes(`${x},${y}`)
    && isOpenSpace(x, y)
    && x >= 0
    && y >= 0;

const countBackwards = (start, end, paths) => {
  let current = end;
  let steps = 0;

  while (current !== start) {
    current = paths[current];
    steps++;
  }

  return steps;
};

const findMinSteps = (startX, startY, endX, endY) => {
  const newPlacesToTry = [`${startX},${startY}`];
  const visited = [];
  const previous = {};

  while (newPlacesToTry.length > 0) {
    const currentLocation = newPlacesToTry.shift();
    const [x, y] = currentLocation.split(',').map(i => i * 1);

    visited.push(currentLocation);

    if (currentLocation === `${endX},${endY}`) break;

    if (isValidSpace(x + 1, y, visited)) {
      const newPlace = `${x + 1},${y}`;

      newPlacesToTry.push(newPlace);
      previous[newPlace] = currentLocation;
    }

    if (isValidSpace(x - 1, y, visited)) {
      const newPlace = `${x - 1},${y}`;

      newPlacesToTry.push(newPlace);
      previous[newPlace] = currentLocation;
    }

    if (isValidSpace(x, y + 1, visited)) {
      const newPlace = `${x},${y + 1}`;

      newPlacesToTry.push(newPlace);
      previous[newPlace] = currentLocation;
    }

    if (isValidSpace(x, y - 1, visited)) {
      const newPlace = `${x},${y - 1}`;

      newPlacesToTry.push(newPlace);
      previous[newPlace] = currentLocation;
    }
  }

  return countBackwards(`${startX},${startY}`, `${endX},${endY}`, previous);
};

const minSteps = findMinSteps(1, 1, 31, 39);

console.log('minimum number of steps', minSteps);
