const input = `rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 5
rect 1x1
rotate row y=0 by 3
rect 1x1
rotate row y=0 by 2
rect 1x1
rotate row y=0 by 3
rect 2x1
rotate row y=0 by 2
rect 1x2
rotate row y=1 by 5
rotate row y=0 by 3
rect 1x2
rotate column x=30 by 1
rotate column x=25 by 1
rotate column x=10 by 1
rotate row y=1 by 5
rotate row y=0 by 2
rect 1x2
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=2 by 18
rotate row y=0 by 5
rotate column x=0 by 1
rect 3x1
rotate row y=2 by 12
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate column x=20 by 1
rotate row y=2 by 5
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=2 by 15
rotate row y=0 by 15
rotate column x=10 by 1
rotate column x=5 by 1
rotate column x=0 by 1
rect 14x1
rotate column x=37 by 1
rotate column x=23 by 1
rotate column x=7 by 2
rotate row y=3 by 20
rotate row y=0 by 5
rotate column x=0 by 1
rect 4x1
rotate row y=3 by 5
rotate row y=2 by 2
rotate row y=1 by 4
rotate row y=0 by 4
rect 1x4
rotate column x=35 by 3
rotate column x=18 by 3
rotate column x=13 by 3
rotate row y=3 by 5
rotate row y=2 by 3
rotate row y=1 by 1
rotate row y=0 by 1
rect 1x5
rotate row y=4 by 20
rotate row y=3 by 10
rotate row y=2 by 13
rotate row y=0 by 10
rotate column x=5 by 1
rotate column x=3 by 3
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate row y=4 by 10
rotate row y=3 by 10
rotate row y=1 by 10
rotate row y=0 by 10
rotate column x=7 by 2
rotate column x=5 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate row y=4 by 20
rotate row y=3 by 12
rotate row y=1 by 15
rotate row y=0 by 10
rotate column x=8 by 2
rotate column x=7 by 1
rotate column x=6 by 2
rotate column x=5 by 1
rotate column x=3 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate column x=46 by 2
rotate column x=43 by 2
rotate column x=24 by 2
rotate column x=14 by 3
rotate row y=5 by 15
rotate row y=4 by 10
rotate row y=3 by 3
rotate row y=2 by 37
rotate row y=1 by 10
rotate row y=0 by 5
rotate column x=0 by 3
rect 3x3
rotate row y=5 by 15
rotate row y=3 by 10
rotate row y=2 by 10
rotate row y=0 by 10
rotate column x=7 by 3
rotate column x=6 by 3
rotate column x=5 by 1
rotate column x=3 by 1
rotate column x=2 by 1
rotate column x=1 by 1
rotate column x=0 by 1
rect 9x1
rotate column x=19 by 1
rotate column x=10 by 3
rotate column x=5 by 4
rotate row y=5 by 5
rotate row y=4 by 5
rotate row y=3 by 40
rotate row y=2 by 35
rotate row y=1 by 15
rotate row y=0 by 30
rotate column x=48 by 4
rotate column x=47 by 3
rotate column x=46 by 3
rotate column x=45 by 1
rotate column x=43 by 1
rotate column x=42 by 5
rotate column x=41 by 5
rotate column x=40 by 1
rotate column x=33 by 2
rotate column x=32 by 3
rotate column x=31 by 2
rotate column x=28 by 1
rotate column x=27 by 5
rotate column x=26 by 5
rotate column x=25 by 1
rotate column x=23 by 5
rotate column x=22 by 5
rotate column x=21 by 5
rotate column x=18 by 5
rotate column x=17 by 5
rotate column x=16 by 5
rotate column x=13 by 5
rotate column x=12 by 5
rotate column x=11 by 5
rotate column x=3 by 1
rotate column x=2 by 5
rotate column x=1 by 5
rotate column x=0 by 1`;

const createRect = (w, h, screen) => {
  const newScreen = screen.slice(0);

  for (let y = 0; y < h; y ++) {
    newScreen[y] = newScreen[y].slice(0);

    for (let x = 0; x < w; x++) {
      newScreen[y][x] = 1;
    }
  }

  return newScreen;
};

const rotateRow =  (which, howMuch, screen) => {
  const newScreen = screen.slice(0);
  const originalRow = screen[which].slice(0);
  const rowLength = originalRow.length;

  newScreen[which] = newScreen[which].slice(0);

  for (let x = 0; x < originalRow.length; x++) {
    newScreen[which][(x + howMuch) % rowLength] = originalRow[x];
  }

  return newScreen;
};

const rotateColumn = (which, howMuch, screen) => {
  const newScreen = screen.slice(0);
  const columnLength = screen.length;

  for (let i = 0; i < columnLength; i++) {
    newScreen[i] = screen[i].slice(0);
  }

  for (let y = 0; y < columnLength; y++) {
    newScreen[(y + howMuch) % columnLength][which] = screen[y][which];
  }

  return newScreen;
};

const rotate = (what, which, howMuch, screen) => {
  if (what === 'column') {
    return rotateColumn(which, howMuch, screen);
  }

  return rotateRow(which, howMuch, screen);
};

const runInstructions = instructions => {
  const WIDTH = 50;
  const HEIGHT = 6;
  const screen = [];

  for (let i = 0; i < HEIGHT; i++) {
    screen.push((new Array(WIDTH)).fill(0))
  }

  return instructions.split('\n').reduce((fin, cur) => {
    const [action, ...specs] = cur.split(' ');

    if (action === 'rect') {
      const dimensions = specs[0].split('x');

      return createRect(...dimensions, fin);
    }

    const [what, which, by, howMuch] = specs;
    const index = which.split('=')[1] * 1;

    return rotate(what, index, howMuch * 1, fin);
  }, screen);
};

const finalScreen = runInstructions(input);

const litPixels = finalScreen.reduce(
  (t1, c1) => t1 + c1.reduce((t2, c2) => t2 + c2, 0)
  , 0);

console.log('number of lit pixels', litPixels);
