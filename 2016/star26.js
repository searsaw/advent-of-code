const isOpenSpace = (x, y) => {
  const FAVE_NUMBER = 1364;
  const z = (x * x) + (3 * x) + (2 * x * y) + y + (y * y) + FAVE_NUMBER;
  const binary = z.toString(2);
  const numberOfOnes = binary.split('').reduce((tot, i) => i === '1' ? tot + 1 : tot, 0);

  return numberOfOnes % 2 === 0;
};

const isValidSpace = (x, y, visited) =>
  !visited.includes(`${x},${y}`)
    && isOpenSpace(x, y)
    && x >= 0
    && y >= 0;

const findAllVisitedPlaces = (startX, startY) => {
  let placesToTry = [`${startX},${startY}`];
  const visited = [];

  for (let i = 0; i < 50; i++) {
    const newPlacesToTry = [];

    while (placesToTry.length > 0) {
      const currentLocation = placesToTry.shift();
      const [x, y] = currentLocation.split(',').map(j => j * 1);

      visited.push(currentLocation);

      [[x + 1, y], [x - 1, y], [x, y + 1], [x, y - 1]].forEach(([_x, _y]) =>
        isValidSpace(_x, _y, visited) && newPlacesToTry.push(`${_x},${_y}`)
      );
    }

    placesToTry = newPlacesToTry;
  }

  return visited.filter((v, i, array) => array.indexOf(v, i + 1) < 0);
};

const visitedPlaces = findAllVisitedPlaces(1, 1);

console.log('max number of visited places within 50 steps', visitedPlaces.length + 1);
