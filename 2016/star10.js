const crypto = require('crypto');

const PASSWORD_LENGTH = 8;
const FILLER_TEXT = 'alex';
const doorId = 'wtnhxymk';
let password = new Array(PASSWORD_LENGTH);
let currentNumber = 0;

password.fill(FILLER_TEXT);

const arrayNotFull = array =>
  array.indexOf(FILLER_TEXT) > -1

const createHash = string =>
  crypto
    .createHash('md5')
    .update(string, 'utf8')
    .digest('hex');

while (arrayNotFull(password)) {
  const possibleHash = createHash(`${doorId}${currentNumber}`);

  if (possibleHash.slice(0, 5) === '00000') {
    const position = possibleHash[5];
    const letter = possibleHash[6];

    if (position < PASSWORD_LENGTH && password[position] === FILLER_TEXT) {
      password[position] = letter;
    }
  }

  currentNumber++;
}

console.log('the door password is', password.join(''));


