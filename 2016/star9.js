const crypto = require('crypto');

const doorId = 'wtnhxymk';
let password = '';
let currentNumber = 0;

while (password.length < 8) {
  const hash = crypto.createHash('md5');

  hash.update(`${doorId}${currentNumber}`, 'utf8');

  const possibleHash = hash.digest('hex');

  if (possibleHash.slice(0, 5) === '00000') {
    password = `${password}${possibleHash[5]}`
  }

  currentNumber++;
}

console.log('the door password is', password);


